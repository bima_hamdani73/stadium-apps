import 'package:flutter/material.dart';
import 'package:stadium_apps/model/stadion.dart';

class DetailScreen extends StatelessWidget {
  final Stadion stadion;

  const DetailScreen({Key? key, required this.stadion});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Image.asset(stadion.gambar),
                SafeArea(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(12, 33, 20, 12),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        stadion.nama,
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Row(children: [
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: Text(
                            'Dibangun',
                            textAlign: TextAlign.left,
                          ),
                          width: 300,
                        ),
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Container(
                        child: Text(
                          ": " + stadion.tahun,
                          textAlign: TextAlign.left,
                        ),
                        width: 300,
                      ),
                      flex: 3,
                    )
                  ]),
                  Row(children: [
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: Text(
                            'Kapasitas',
                            textAlign: TextAlign.left,
                          ),
                          width: 300,
                        ),
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Container(
                        child: Text(
                          ": " + stadion.kapasitas + " Penonton",
                          textAlign: TextAlign.left,
                        ),
                        width: 300,
                      ),
                      flex: 3,
                    )
                  ]),
                  Row(children: [
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: Text(
                            'Lokasi',
                            textAlign: TextAlign.left,
                          ),
                          width: 300,
                        ),
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Container(
                        child: Text(
                          ": " + stadion.lokasi,
                          textAlign: TextAlign.left,
                        ),
                        width: 300,
                      ),
                      flex: 3,
                    )
                  ])
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 20, bottom: 14),
              child: Text(
                "Sejarah",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      stadion.deskripsi,
                      textAlign: TextAlign.justify,
                    ))),
            Container(
              margin: const EdgeInsets.only(top: 18, left: 20, bottom: 6),
              child: Text(
                "Layanan dan Fasilitas",
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 12),
              height: 150,
              child: ListView(
                children: stadion.fasilitas.map((url) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(url),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
