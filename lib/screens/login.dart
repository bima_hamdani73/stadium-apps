import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:stadium_apps/helper/theme_helper.dart';
import 'package:stadium_apps/screens/main_screen.dart';
import 'package:stadium_apps/screens/registration.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Key _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#ced6e0"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 297,
              height: 90,
              margin: EdgeInsets.fromLTRB(48, 46, 48, 42),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: HexColor("#1e90ff"),
              ),
              child: Center(
                child: Text(
                  'Login',
                  style: TextStyle(
                      fontSize: 60,
                      fontFamily: 'FrederickatheGreat',
                      color: HexColor("#FCF9F9")),
                ),
              ),
            ),
            SafeArea(
              child: Container(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                  margin: EdgeInsets.fromLTRB(
                      20, 0, 20, 10), // This will be the login form
                  child: Column(
                    children: [
                      CircleAvatar(
                          radius: 82,
                          backgroundColor: Colors.amber,
                          child: CircleAvatar(
                            radius: 80,
                            backgroundImage: AssetImage('assets/st_gbk.jpg'),
                          )),
                      SizedBox(height: 8.0),
                      Text(
                        'Signin into your account',
                        style: TextStyle(color: HexColor("#2f3542")),
                      ),
                      SizedBox(height: 30.0),
                      Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                child: Text(
                                  'Username',
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontFamily: 'FredokaOne',
                                      color: HexColor("#FCF9F9")),
                                ),
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Container(
                                child: TextField(
                                  decoration: ThemeHelper().textInputDecoration(
                                      'User Name', 'Enter your user name'),
                                ),
                                decoration:
                                    ThemeHelper().inputBoxDecorationShaddow(),
                              ),
                              SizedBox(height: 30.0),
                              Container(
                                child: Text(
                                  'Password',
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontFamily: 'FredokaOne',
                                      color: HexColor("#FCF9F9")),
                                ),
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Container(
                                child: TextField(
                                  obscureText: true,
                                  decoration: ThemeHelper().textInputDecoration(
                                      'Password', 'Enter your password'),
                                ),
                                decoration:
                                    ThemeHelper().inputBoxDecorationShaddow(),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
                                child: Text.rich(TextSpan(children: [
                                  TextSpan(
                                      text: "Don't have any account ? ",
                                      style: TextStyle(
                                          fontFamily: 'GowunBatang',
                                          fontSize: 14,
                                          color: HexColor("#2f3542"))),
                                  TextSpan(
                                    text: 'Register',
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    RegistrationPage()));
                                      },
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: HexColor("#8c8c8c")),
                                  ),
                                ])),
                              ),
                              SizedBox(height: 35.0),
                              Container(
                                decoration:
                                    ThemeHelper().buttonBoxDecoration(context),
                                child: ElevatedButton(
                                  style: ThemeHelper().buttonStyle(),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.fromLTRB(40, 10, 40, 10),
                                    child: Text(
                                      'Sign In'.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                MainScreen()));
                                  },
                                ),
                              ),
                            ],
                          )),
                      SizedBox(height: 41.0),
                      Container(
                        child: Text(
                          "copyright by Bima",
                          style: TextStyle(
                              fontFamily: 'GowunBatang',
                              fontSize: 14,
                              color: HexColor("#2f3542")),
                        ),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
