import 'package:flutter/material.dart';
import 'package:stadium_apps/screens/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: "Stadion", home: LoginPage());
  }
}
