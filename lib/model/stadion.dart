class Stadion {
  String gambar;
  String nama;
  String tahun;
  String kapasitas;
  String lokasi;
  String deskripsi;
  String pulau;
  List<String> fasilitas;

  Stadion({
    required this.gambar,
    required this.nama,
    required this.tahun,
    required this.kapasitas,
    required this.lokasi,
    required this.deskripsi,
    required this.fasilitas,
    required this.pulau,
  });
}

var stadionList = [
  Stadion(
    gambar: 'st_gbk.jpg',
    nama: 'Stadion Gelora Bung Karno',
    tahun: '1955',
    kapasitas: '100.000',
    lokasi: 'Jakarta',
    pulau: 'Jawa',
    deskripsi:
        'Stadion ini adalah stadion Indonesia yang sudah berstandar FIFA, atau sudah masuk ke stadion internasional.Tidak hanya stadionnya saja yang sudah masuk skala Internasional, akan tetapi komplek olahraga yang berada di dalam stadion juga telah masuk taraf internasional.',
    fasilitas: ['Kolam Renang', 'Persija Store', 'Gym'],
  ),
  Stadion(
    gambar: 'st_gbla.jpg',
    nama: 'Stadion GBLA',
    tahun: '2012',
    kapasitas: '38.000',
    lokasi: 'Bandung, Jawa Barat',
    pulau: 'Jawa',
    deskripsi:
        'Stadion Gelora Bandung Lautan Api adalah stadion baru yang menjadi kandang Persib Bandung. Tipe rumput yang digunakan pada stadion ini adalah rumput yang berjenis Zoycia Matrella [Linn] Merr, salah satu rumput kelas satu standar FIFA',
    fasilitas: ['Persib Store', 'Lapangan Latihan', 'Cafe'],
  ),
  Stadion(
    gambar: 'st_palaran.jpg',
    nama: 'Stadion Utama Palaran',
    tahun: '2005',
    kapasitas: '60.000',
    lokasi: 'Samarinda, Kalimantan Timur.',
    pulau: 'Kalimantan',
    deskripsi:
        'Klub Persisam dulu telah menjadikan Stadion Utama Palaran sebagai kandang mereka. Biaya yang dikeluarkan untuk membangun stadion megah ini kurang lebih mencapai 800 miliyar.',
    fasilitas: ['Kolam Renang', 'Lapangan Golf'],
  ),
  Stadion(
    gambar: 'st_aji.jpg',
    nama: 'Stadion Aji Imbut',
    tahun: '2005',
    kapasitas: '35.000',
    lokasi: 'Kutai Kartanegara, Kalimantan Timur',
    pulau: 'Kalimantan',
    deskripsi:
        'Tipe stadion ini adalah tipe olimpik. Stadion ini memiliki fasilitas yang bagus dan memadai serta menjadi salah satu stadion yang bertaraf internasional.',
    fasilitas: ['Kolam Renang', 'Cafe', 'Gym', 'Store'],
  ),
  Stadion(
    gambar: 'st_riau.jpg',
    nama: 'Stadion Utama Riau',
    tahun: '2009',
    kapasitas: '44.000',
    lokasi: 'Pekanbaru, Riau',
    pulau: 'Sumatera',
    deskripsi:
        'Stadion Utama Riau adalah sebuah stadion serbaguna di Pekanbaru, Riau, Indonesia. Setelah selesai pada tahun 2012, maka akan digunakan terutama untuk pertandingan sepak bola dan akan menjadi tuan rumah upacara pembukaan dan penutupan Pekan Olahraga Nasional 2012.',
    fasilitas: ['Kolam Renang', 'Gym'],
  ),
  Stadion(
    gambar: 'st_bungtomo.jpg',
    nama: 'Stadion Gelora Bung Tomo',
    tahun: '2008',
    kapasitas: '55.000',
    lokasi: 'Surabaya, Jawa Timur',
    pulau: 'Jawa',
    deskripsi:
        'Stadion ini begitu luas dan megah, maka lampu yang digunakan untuk menarangi stadion ini sebesar 1600 lux. Dengan kekuatan lampu sebesar itu, maka tidak heran saat pertandingan malam, cahaya yang dihasilkan begitu terang sehingga nyaman untuk bermain. Selain itu, jenis rumput yang digunakan untuk lapangan ini adalah jenis Zoycia matrella.',
    fasilitas: ['Cafe', 'Lapangan Latihan', 'Gym'],
  ),
  Stadion(
    gambar: 'st_maguwo.jpg',
    nama: 'Stadion Maguwoharjo',
    tahun: '2005',
    kapasitas: '31.700',
    lokasi: 'Sleman, Jogjakarta',
    pulau: 'Jawa',
    deskripsi:
        'Konsep bangunan yang digunakan pada stadion ini adalah tipe modern dengan julukan " mini san siro " dengan ciri khas yang dimiliki oleh stadion ini memiliki 4 menara yang terletak di penjuru stadion. Seperti halnya stadion yang berada di Eropa, stadion ini juga tidak memiliki lintasan atletik sehingga penonton merasa nyaman dan bisa lebih dekat dengan pemain.',
    fasilitas: ['Kolam Renang', 'Sleman Store', 'Gym'],
  ),
  Stadion(
    gambar: 'st_jakabaring.jpg',
    nama: 'Stadion Jakabaring',
    tahun: '2002',
    kapasitas: '36.000',
    lokasi: 'Palembang, Sumatera Selatan',
    pulau: 'Sumatera',
    deskripsi:
        'Stadion Gelora Sriwijaya, juga dikenal sebagai Stadion Jakabaring, adalah sebuah multiguna stadion terletak di kompleks Jakabaring Sport City di Palembang, Sumatera Selatan, Indonesia. Saat ini sebagian besar digunakan untuk pertandingan sepak bola. ',
    fasilitas: ['Kolam Renang', 'Store', 'Gym', 'Lapangan Latihan'],
  ),
  Stadion(
    gambar: 'st_patriot.jpg',
    nama: 'Stadion Patriot',
    tahun: '1980',
    kapasitas: '30.000',
    lokasi: 'Bekasi, Jawa Barat',
    pulau: 'Jawa',
    deskripsi:
        'Stadion ini digunakan sebagai awal untuk pekan Olahraga Daerah (Porda) Jawa Barat yang ke IV pada tahun 1984. Setelah lama digunakan, maka stadion ini mulai direnovasi pada tahun 2012 dan pada tanggal 11 Maret tahun 2014 mulai diresmikan.',
    fasilitas: ['Kolam Renang', 'Cafe', 'Gym'],
  ),
  Stadion(
    gambar: 'st_pakansari.jpg',
    nama: 'Stadion Pakansari',
    tahun: '2012',
    kapasitas: '31.000',
    lokasi: 'Bogor, Jawa Barat',
    pulau: 'Jawa',
    deskripsi:
        'Stadion Pakansari pernah menggelar pertandingan Timnas Indonesia beberapa kali sejak awal berdirinya stadion ini. Klub yang pernah menjadikan Stadion Pakansari sebagai kandang mereka adalah Persikabo Bogor dan PS TIRA/PS TNI.',
    fasilitas: ['Store', 'Lapangan Latihan', 'Gym'],
  ),
  Stadion(
    gambar: 'st_bali.jpg',
    nama: 'Stadion I Wayan Dipta',
    tahun: '2003',
    kapasitas: '23.081',
    lokasi: 'Gianyar, Bali',
    pulau: 'Jawa',
    deskripsi:
        'Stadion Kapten I Wayan Dipta adalah sebuah stadion multifungsi di Gianyar, Bali, Indonesia dengan kapasitas 25.000 kursi penonton. Fungsi utama stadion ini adalah untuk menyelenggarakan pertandingan sepak bola. Dahulu stadion ini merupakan markas kesebelasan asal Gianyar, yakni Persegi Gianyar.',
    fasilitas: [
      'Bali United Store',
      'Bali United Cafe',
      'Bali United Playland'
    ],
  ),
  Stadion(
    gambar: 'st_agus.jpg',
    nama: 'Stadion H. Agus Salim',
    tahun: '1985',
    kapasitas: '25.000',
    lokasi: 'Padang, Sumatera Barat',
    pulau: 'Sumatera',
    deskripsi:
        'Dibangun oleh Pemerintah Daerah Tk. I Sumatera Barat itu dimaksudkan untuk persiapan pelaksanaan MTQ ke 13 pada tahun 1983 namun hanya tribun Barat dan tribun Selatan',
    fasilitas: [
      'Semen Padang Store',
      'Kolam renang',
    ],
  ),
  Stadion(
    gambar: 'st_mauli.jpg',
    nama: 'Stadion Mauli Warmadewa',
    tahun: '2014',
    kapasitas: '30.000',
    lokasi: 'Dharmasraya, Sumatera Barat',
    pulau: 'Sumatera',
    deskripsi:
        'Nama stadion tersebut diambil dari nama salah seorang Maharaja Melayu yang pernah memerintah di Kabupaten Dharmasraya, Srimat Tribhuwanaraja Mauli Warmadewa, pada tahun 1286–1316.',
    fasilitas: [
      'Kantin',
      'Kolam renang',
    ],
  ),
  Stadion(
    gambar: 'st_batakan.jpg',
    nama: 'Stadion Batakan',
    tahun: '2020',
    kapasitas: '40.000',
    lokasi: 'Balikpapan, Kalimantan Timur',
    pulau: 'Kalimantan',
    deskripsi:
        'Stadion Batakan Balikpapan adalah sebuah stadion sepak bola di Balikpapan, Kalimantan Timur, Indonesia. Stadion ini menjadi tuan rumah klub Liga 2 Persiba Balikpapan. Stadion ini memiliki kapasitas 40.000 orang.',
    fasilitas: [
      'Gym',
      'Kolam renang',
      'Lapangan latihan',
    ],
  ),
  Stadion(
    gambar: 'st_habibie.jpg',
    nama: 'Stadion Bj Habibie',
    tahun: '2015',
    kapasitas: '15.000',
    lokasi: 'Pare-pare, Sulawesi Selatan',
    pulau: 'Sulawesi',
    deskripsi:
        'Nama stadion tersebut diambil dari nama salah seorang Maharaja Melayu yang pernah memerintah di Kabupaten Dharmasraya, Srimat Tribhuwanaraja Mauli Warmadewa, pada tahun 1286–1316.',
    fasilitas: [
      'Kantin',
      'Kolam renang',
    ],
  ),
  Stadion(
    gambar: 'st_gangga.jpg',
    nama: 'Stadion Ganggawa',
    tahun: '2019',
    kapasitas: '10.000',
    lokasi: 'Sidrap, Sulawesi Selatan',
    pulau: 'Sulawesi',
    deskripsi:
        'Nama stadion tersebut diambil dari nama salah seorang Maharaja Melayu yang pernah memerintah di Kabupaten Dharmasraya, Srimat Tribhuwanaraja Mauli Warmadewa, pada tahun 1286–1316.',
    fasilitas: [
      'Kantin',
      'Gym',
    ],
  ),
  Stadion(
    gambar: 'st_mata.jpg',
    nama: 'Stadion Andi Matalata',
    tahun: '1957',
    kapasitas: '15.000',
    lokasi: 'Makassar, Sulawesi Selatan',
    pulau: 'Sulawesi',
    deskripsi:
        'Stadion Andi Mattalatta adalah sebuah stadion di Makassar, Sulawesi Selatan, Indonesia. Stadion ini lebih sering dipergunakan untuk menggelar pertandingan sepak bola dan merupakan kandang dari tim kebangaan rakyat Makassar PSM Makassar',
    fasilitas: [
      'Kantin',
      'Gym',
    ],
  ),
  Stadion(
    gambar: 'st_lukas.jpg',
    nama: 'Stadion Lukas Enembe',
    tahun: '2019',
    kapasitas: '40.000',
    lokasi: 'Jayapura, Papua',
    pulau: 'Papua',
    deskripsi:
        'Stadion Lukas Enembe , sebelumnya bernama Stadion Papua Bangkit ( Papua Rising Stadium ), adalah sebuah stadion di Kabupaten Jayapura , Papua .',
    fasilitas: [
      'Kantin',
      'Gym',
      'Papua Store',
    ],
  ),
  Stadion(
    gambar: 'st_mandala.jpg',
    nama: 'Stadion Mandala',
    tahun: '2019',
    kapasitas: '30.000',
    lokasi: 'Jayapura, Papua',
    pulau: 'Papua',
    deskripsi:
        'Stadion Mandala adalah sebuah stadion serbaguna di Jayapura , Papua , Indonesia . Didirikan pada tahun 1950 sebagai Lapangan Dock V, saat ini sebagian besar digunakan untuk pertandingan sepak bola. ',
    fasilitas: [
      'Kantin',
      'Gym',
    ],
  ),
  Stadion(
    gambar: 'st_barna.jpg',
    nama: 'Stadion Barnabas Youwe',
    tahun: '1957',
    kapasitas: '15.000',
    lokasi: 'Jayapura, Papua',
    pulau: 'Papua',
    deskripsi:
        'Stadion Barnabas Youwe adalah sebuah stadion serbaguna di Kabupaten Jayapura , Papua , Indonesia . Hal ini terutama digunakan sebagian besar untuk pertandingan sepak bola. Stadion ini memiliki kapasitas 15.000 penonton',
    fasilitas: [
      'Kantin',
      'Gym',
    ],
  ),
];
